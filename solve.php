<?php

class Location {

    public $latitude;
    public $longitude;
    public $id;

    public function __construct($id = null, $latitude, $longitude) {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->id = $id;
    }

    public static function getInstance($location) {
        $location = (array) $location;
        if (empty($location['latitude']) || empty($location['longitude'])) {
            throw new RuntimeException('Location could not load location');
        }

        $id = isset($location['id']) ? $location['id'] : null;
        $tsLocation = new Location($id, $location['latitude'], $location['longitude']);

        return $tsLocation;
    }

    public static function distance($lat1, $long1, $lat2, $long2) {
        if ($lat1 == $lat2 && $long1 == $long2)
            return 0;

        $theta = $long1 - $long2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return $miles;
    }

}

class Node {

    /**
     * @param   array    $parentMatrix   The parentMatrix of the costMatrix.
     * @param   array    $path           An array of integers for the path.
     * @param   integer  $level          The level of the node.
     * @param   integer  $i, $j          They are corresponds to visiting city j from city i
     */
    public $path = array();
    public $reducedMatrix = array();
    public $cost;
    public $vertex;
    public $level;

    public function __construct($parentMatrix, $path, $level, $i, $j) {
        // stores ancestors edges of state space tree
        $this->path = $path;

        // skip for root node
        if ($level != 0)
        // add current edge to path
            $this->path[] = array($i, $j);

        // copy data from parent node to current node
        $this->reducedMatrix = $parentMatrix;

        // Change all entries of row i and column j to infinity
        // skip for root node
        for ($k = 0; $level != 0 && $k < count($parentMatrix); $k++) {
            // set outgoing edges for city i to infinity
            $this->reducedMatrix[$i][$k] = INF;
            // set incoming edges to city j to infinity
            $this->reducedMatrix[$k][$j] = INF;
        }

        // Set (j, 0) to infinity 
        // here start node is 0
        $this->reducedMatrix[$j][0] = INF;

        $this->level = $level;
        $this->vertex = $j;
    }

}

class CustomCompare extends SplPriorityQueue {

    public function compare($lhs, $rhs) {
        if ($lhs === $rhs)
            return 0;
        return ($lhs < $rhs) ? 1 : -1;
    }

}

class BranchBound {

    protected $n = 0;
    protected $locations = array();
    protected $costMatrix = array();

    /**
     * array BranchBound instances container.
     */
    protected static $instances = array();

    public function __construct($costMatrix = array()) {
        if ($costMatrix) {
            $this->costMatrix = $costMatrix;
            $this->n = count($this->costMatrix);
        }
    }

    /**
     * Method to get an instance of a BranchBound.
     *
     * @param   string  $name     The name of the BranchBound.
     * @param   array   $locations  An array of locations.
     *
     * @return  object  BranchBound instance.
     *
     */
    public static function getInstance($name = 'BranchBound', $locations = null) {
        $instances = &self::$instances;
        if (!isset($instances[$name])) {
            $instances[$name] = new BranchBound();
        }

        $instances[$name]->locations = array();
        $instances[$name]->costMatrix = array();

        if ($locations) {
            if ($instances[$name]->load($locations) == false) {
                throw new RuntimeException('BranchBound: could not load locations');
            }
        }

        return $instances[$name];
    }

    public function load($locations) {
        if (empty($locations))
            return false;

        foreach ($locations as $location) {
            if (empty($location))
                return false;

            if ($this->addLocation($location) == false)
                return false;
        }

        return $this->loadMatrix();
    }

    public function loadMatrix() {
        if (empty($this->locations))
            return false;

        $this->costMatrix = array();
        $n_locations = count($this->locations);
        for ($i = 0; $i < $n_locations; $i++) {
            for ($j = 0; $j < $n_locations; $j++) {
                $distance = INF;
                if ($i != $j) {
                    $loc1 = $this->locations[$i];
                    $loc2 = $this->locations[$j];
                    $distance = Location::distance($loc1->latitude, $loc1->longitude, $loc2->latitude, $loc2->longitude);
                }
                $this->costMatrix[$i][$j] = $distance;
            }
        }

        $this->n = count($this->costMatrix);

        return true;
    }

    public function addLocation($location) {
        try {
            $location = Location::getInstance($location);
        } catch (Exception $e) {
            print_r($e);
            echo "<br>";
            return false;
        }

        $this->locations[] = $location;
        return true;
    }

    protected function rowReduction(&$reducedMatrix, &$row) {
        // initialize row array to INF 
        $row = array_fill(0, $this->n, INF);

        // row[i] contains minimum in row i
        for ($i = 0; $i < $this->n; $i++)
            for ($j = 0; $j < $this->n; $j++)
                if ($reducedMatrix[$i][$j] < $row[$i])
                    $row[$i] = $reducedMatrix[$i][$j];

        // reduce the minimum value from each element in each row.
        for ($i = 0; $i < $this->n; $i++)
            for ($j = 0; $j < $this->n; $j++)
                if ($reducedMatrix[$i][$j] !== INF && $row[$i] !== INF)
                    $reducedMatrix[$i][$j] -= $row[$i];
    }

    protected function columnReduction(&$reducedMatrix, &$col) {
        // initialize row array to INF 
        $col = array_fill(0, $this->n, INF);

        // col[i] contains minimum in row i
        for ($i = 0; $i < $this->n; $i++)
            for ($j = 0; $j < $this->n; $j++)
                if ($reducedMatrix[$i][$j] < $col[$j])
                    $col[$j] = $reducedMatrix[$i][$j];

        // reduce the minimum value from each element in each row.
        for ($i = 0; $i < $this->n; $i++)
            for ($j = 0; $j < $this->n; $j++)
                if ($reducedMatrix[$i][$j] !== INF && $col[$j] !== INF)
                    $reducedMatrix[$i][$j] -= $col[$j];
    }

    protected function calculateCost(&$reducedMatrix) {
        // initialize cost to 0
        $cost = 0;

        // Row Reduction
        $row = array();
        $this->rowReduction($reducedMatrix, $row);

        // Column Reduction
        $col = array();
        $this->columnReduction($reducedMatrix, $col);

        // the total expected cost 
        // is the sum of all reductions
        for ($i = 0; $i < $this->n; $i++) {
            $cost += ($row[$i] !== INF) ? $row[$i] : 0;
            $cost += ($col[$i] !== INF) ? $col[$i] : 0;
        }

        return $cost;
    }

    public function printPath($list) {
        for ($i = 0; $i < count($list)-1; $i++) {
            $start = (int) $list[$i][0];
            $end = (int) $list[$i][1];
            if ($i == 0)
                echo $this->locations[$start]->id . '<br>' . $this->locations[$end]->id . "<br>";
            else {
                echo $this->locations[$end]->id . "<br>";
            }
        }
    }

    public function solve() {
        if (empty($this->costMatrix)) {
            if (!$this->loadMatrix())
                return false;
        }

        $costMatrix = $this->costMatrix;
        // Create a priority queue to store live nodes of
        // search tree;
        $pq = new CustomCompare();

        // create a root node and calculate its cost
        // The TSP starts from first city i.e. node 0
        $root = new Node($costMatrix, null, 0, -1, 0);
        // get the lower bound of the path starting at node 0
        $root->cost = $this->calculateCost($root->reducedMatrix);

        // Add root to list of live nodes;
        $pq->insert($root, $root->cost);

        // Finds a live node with least cost,
        // add its children to list of live nodes and
        // finally deletes it from the list.
        while ($pq->valid()) {
            // Find a live node with least estimated cost
            $min = $pq->extract();

            // Clear the max estimated nodes
            $pq = new CustomCompare();

            // i stores current city number
            $i = $min->vertex;

            // if all cities are visited
            if ($min->level == $this->n - 1) {
                // return to starting city
                $min->path[] = array($i, 0);
                // print list of cities visited;
                $this->printPath($min->path);

                // return optimal cost & etc.
                return array('cost' => $min->cost, 'path' => $min->path, 'locations' => $this->locations);
            }

            // do for each child of min
            // (i, j) forms an edge in space tree
            for ($j = 0; $j < $this->n; $j++) {
                if ($min->reducedMatrix[$i][$j] !== INF) {
                    // create a child node and calculate its cost
                    $child = new Node($min->reducedMatrix, $min->path, $min->level + 1, $i, $j);

                    /* Cost of the child = 
                      cost of parent node +
                      cost of the edge(i, j) +
                      lower bound of the path starting at node j
                     */
                    $child->cost = $min->cost + $min->reducedMatrix[$i][$j] + $this->calculateCost($child->reducedMatrix);

                    // Add child to list of live nodes
                    $pq->insert($child, $child->cost);
                }
            }

            // free node as we have already stored edges (i, j) in vector
            // So no need for parent node while printing solution.
            $min = null;
        }
    }

}

try {
    $tsp = BranchBound::getInstance();
    $cities = fopen("cities.txt", "r");
    while (!feof($cities)) {
        $city = fgets($cities);
        $arrCity = explode(' ', $city);
        if (count($arrCity) == 3)
            $tsp->addLocation(array('id' => $arrCity[0], 'latitude' => $arrCity[1], 'longitude' => $arrCity[2]));
        else if (count($arrCity) == 4)
            $tsp->addLocation(array('id' => $arrCity[0] . ' ' . $arrCity[1], 'latitude' => $arrCity[2], 'longitude' => $arrCity[3]));
    }
    fclose($cities);
    $ans = $tsp->solve();
} catch (Exception $e) {
    echo $e;
    exit;
}
?>